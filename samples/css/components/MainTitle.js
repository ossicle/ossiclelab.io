import {Style, Template, Ossicle} from '../js/Ossicle.js'

class MainTitle extends Ossicle {

  static sheets() { return [
    Style.sheets.bulma, Style.sheets.colors
  ]}

  static template() { return Template.html`
    <div>
      <h1 class="title orange">👋 Hello World 🌍</h1>
      <h2 class="subtitle green">with Ossicle.js</h2>
    </div>
  `}  

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-title', MainTitle)
