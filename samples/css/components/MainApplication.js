import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

class MainApplication extends Ossicle {

  static sheets() { return [
    Style.sheets.bulma
  ]}

  static template() { return Template.html`
    <div class="container mb-2 pt-2">
      <div class="section">
        <main-title></main-title>
      </div>
      <div class="section">
        <p>
          Hello from the "MainApplication"
        </p>
      </div>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-application', MainApplication)
