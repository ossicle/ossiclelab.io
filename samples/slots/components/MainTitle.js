import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div>
    <h1 class="title orange"></h1>
    <h2 class="subtitle green"></h2>
    <hr>
      <slot></slot>
    <hr>
    <b><slot name="bold-slot"></slot></b>
  </div>
`

class MainTitle extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma, Style.sheets.colors])
  }

  connectedCallback() {
    this.$("h1").innerText = this.getAttribute("title")
    this.$("h2").innerText = this.getAttribute("sub-title")
  }
}

customElements.define('main-title', MainTitle)
