import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title title="Hello World" sub-title="with Ossicle">
        this is an amazing web application 😉
        <span slot="bold-slot">this is a bold sentence</span>
      </main-title>
    </div>

    <div class="section">
      <p>
        Hello from the "MainApplication"
      </p>
    </div>

  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }
}

customElements.define('main-application', MainApplication)
