import {Style, Template, Ossicle} from '../js/Ossicle.js'


class MainTitle extends Ossicle {

  static sheets() { return [Style.sheets.bulma, Style.sheets.colors] }

  static template() { return Template.html`
    <div>
      <h1 class="title orange"></h1>
      <h2 class="subtitle green"></h2>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }

  connectedCallback() {
    /*
    this.$("div").appendChild(Template.html`
      <h1 class="title orange">${this.getAttribute("title")}</h1>
      <h2 class="subtitle green">${this.getAttribute("sub-title")}</h2>
    `.clone())
    */

    this.$("h1").innerText = this.getAttribute("title")
    this.$("h2").innerText = this.getAttribute("sub-title")
  }
}

customElements.define('main-title', MainTitle)
