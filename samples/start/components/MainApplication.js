import {Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <h1>
    👋 Hello World 🌍
  </h1>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.appendChild(template)
  }
}

customElements.define('main-application', MainApplication)
