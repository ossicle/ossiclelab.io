import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <button class="button is-info">
    <slot></slot>
  </button>
`

class NiceButton extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }
}

customElements.define('nice-button', NiceButton)
