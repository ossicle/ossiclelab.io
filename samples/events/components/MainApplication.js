import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'
import './NiceButton.js'

const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title>Hello World</main-title>
    </div>

    <div class="section">
      <nice-button name="btn1">one</nice-button>
      <nice-button name="btn2">two</nice-button>
    </div>

  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {
    this.$name("btn1").addEventListener("click", _ => {
      // update the slot content of <main-title>
      //this.$("main-title").innerText = "You clicked button one"
      this.$("main-title").title = "You clicked button one 👋"
    })

    this.$name("btn2").addEventListener("click", _ => {
      // update the slot content of <main-title>
      //this.$("main-title").innerText = "You clicked button two"
      this.$("main-title").title =  "You clicked button two 👋"
    })

  }
}

customElements.define('main-application', MainApplication)
