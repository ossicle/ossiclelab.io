import {Style, Template, Router, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title>Hello World</main-title>
    </div>

    <div class="section">
      <ul>
        <li><a href="#/one">One</a></li>
        <li><a href="#/two">Two</a></li>
        <li><a href="#/three">Three</a></li>
      </ul>
    </div>

  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {
    // start the router and call `onRouterEvent` at every `hash` event
    // the firs time the router triggers a `load` event
    Router.start(message => this.onRouterEvent(message))
  }

  onRouterEvent(message) {
    console.log("type", message.type)
    console.log("location", message.location)
  }

}

customElements.define('main-application', MainApplication)
