import {Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'


class MainApplication extends Ossicle {

  static template() { return Template.html`
    <div>
      <main-title></main-title>
      <div>
        👋 Hello from the "MainApplication"
      </div>
      <list></list>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }

  connectedCallback() {
    var greetings = ["Hello", "Morgen", "Bonjour"]

    this.$("list").appendChild(Template.html`
        <ul id="first-list">
          ${greetings.map(item => `<li>${item}</li>`).join("")}
        </ul>
      `.clone()
    )

    /*
    greetings = ["Hey", "Yo", "Hi"]

    this.$("list").replaceChild(Template.html`
        <ul id="new-list">
          ${greetings.map(item => `<li>${item}</li>`).join("")}
        </ul>
      `.clone(),
      this.$id("first-list")
    )

    this.$("list").removeChild(this.$id("new-list"))
    */

  }

}

customElements.define('main-application', MainApplication)
