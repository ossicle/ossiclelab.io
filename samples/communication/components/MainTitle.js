import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div>
    <h1 class="title">👋 <slot></slot></h1>
  </div>
`

class MainTitle extends Ossicle {

  set title(value) {
    this.innerText = value
  }

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

}

customElements.define('main-title', MainTitle)
