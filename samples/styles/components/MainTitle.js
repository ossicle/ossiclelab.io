import {Style, Template, Ossicle} from '../js/Ossicle.js'

class MainTitle extends Ossicle {

  static sheets() { return [
    Style.css`
      h1 { color: orange; }
      h2 { color: green; }
    `
  ]}

  static template() { return Template.html`
    <div>
      <h1>👋 Hello World 🌍</h1>
      <h2>with Ossicle.js</h2>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-title', MainTitle)
