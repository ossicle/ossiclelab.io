import {Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

class MainApplication extends Ossicle {
  static template() { return Template.html`
    <div>
      <main-title></main-title>
      <div>
        Hello from the "MainApplication"
      </div>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-application', MainApplication)
