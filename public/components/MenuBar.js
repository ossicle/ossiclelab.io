import {Style, Template, Fragment, Ossicle} from '../node_modules/@ossicle/ossicle.js/Ossicle.js'
import {Data} from '../content/Data.js'


class MenuBar extends Ossicle {

  static sheets() { return [
    Style.sheets.bulma
  ]}

  static template() { return Template.html`
    <aside class="menu">
      <p class="menu-label">
        Documentation
      </p>
      <ul class="menu-list">
        ${Data.docSections.map(section => Fragment.html`
          <li>
            <a id="${section.id}" href="#/${section.id}" class="tab is-active">${section.title}</a>
          </li>
        `).join("")}
      </ul>
    </aside>
    `
  }

  activate(id) {
    this.$classes("tab").forEach(tab => tab.className = "tab")
    this.$id(id).className += " is-active"
  }

  constructor() {
    super()
    //this.setup(template.clone(), [Style.sheets.bulma])
    this.setup()
  }
}

customElements.define('menu-bar', MenuBar)
