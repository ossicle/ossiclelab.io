import {Style, Template, Ossicle} from '../node_modules/@ossicle/ossicle.js/Ossicle.js'


const template = Template.html`
  <div class="container">
    <content>
      <h1>⏳ loading...</h1>
    </content>
  </div>
`

class MarkDown extends Ossicle {

  static sheets() { return [
    Style.sheets.highlight,
    Style.sheets.purple,
    Style.css`
      code {
        font-family: Menlo;
        border-radius: 8px;
      }
      p {
        font-size:1.1em;
      }
    `
  ]}

  static template() { return Template.html`
    <div class="container">
      <content>
        <h1>⏳ loading...</h1>
      </content>
    </div>
  `}


  constructor() {
    super()
    this.setup()
    /*
    this.setup(template.clone(), [
      //Style.sheets.bulma,
      Style.sheets.highlight,
      Style.sheets.purple,
      sheetCode
    ])
    */
  }

  initialize() {

    fetch(this.getAttribute("file"))
      .then(response => response.text())
      .then(mdText => {
        //console.log(mdText)
        this.$("content").innerHTML = window.markdownit().render(mdText)

        this.$all('code').forEach(code => {
          //console.log(code.style)
          //code.style.fontFamily="Menlo"
          hljs.highlightBlock(code)
        })
        //console.log(this.$all("blockquote"))
        //this.$all("blockquote").forEach(quote => quote.className="notification is-info")
      })

  }

  connectedCallback() {
    this.initialize()
  }


}

customElements.define('mark-down', MarkDown)
