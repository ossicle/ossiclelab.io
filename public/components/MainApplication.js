import {Style, Template, Router, Ossicle, Fragment} from '../node_modules/@ossicle/ossicle.js/Ossicle.js'
import './MainTitle.js'
import './MarkDown.js'
//import './TabsBar.js'
import './MenuBar.js'
import {Data} from '../content/Data.js'

class MainApplication extends Ossicle {

  static template() { return Template.html`
    <main-title></main-title>

    <div class="container mb-2 pt-2">
      <article class="media">
        <div class="media-left">
          <menu-bar></menu-bar>
        </div>

        <div class="media-content">
          ${Data.docSections.map(section => Fragment.html`
            <tab id="${section.id}" class="container mb-2 pt-2" style="display:none">
              <mark-down file="${section.file}"></mark-down>
            </tab>
          `).join("")}

          <div class="container mb-2 pt-2">
            <!-- add a footer -->
            <hr>
          </div>

        </div>

        <div class="media-right">
          <p>
          <!--
            Horizontal padding
          -->
          </p>
        </div>

      </article>
    </div>

    <!--
    <section class="section">
    </section>
    -->
  `}

  static sheets() { return [
    Style.sheets.bulma
  ]}

  constructor() {
    super()
    //this.setup(template.clone(), [Style.sheets.bulma])
    this.setup()
  }

  connectedCallback() {
    // start the router and call `onRouterEvent` at every `hash` event
    // the firs time the router triggers a `load` event
    Router.start(message => this.onRouterEvent(message))
  }

  onRouterEvent(message) {
    /*
    if(message.type==='load') {}
    if(message.type==='hashchange') {}
    */

    let tabId = message.location === "/" ? "intro" : message.location.slice(1)

    this.$all("tab").forEach(tab => tab.style.display="none")

    this.$id(tabId).style.display = "block"

    this.$("menu-bar").activate(tabId)
    //this.$("tabs-bar").activate(tabId)
  }
}

customElements.define('main-application', MainApplication)
