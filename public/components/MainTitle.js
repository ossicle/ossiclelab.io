import {Style, Template, Ossicle} from '../node_modules/@ossicle/ossicle.js/Ossicle.js'
import {Data} from '../content/Data.js'


const mainTitle = "Ossicle"
const subTitle = "Ease the development of applications with web components - 0 dependency - 0 bundler - 0 tool | 4.02 KB | minified: 1.79 KB"
const project = "https://gitlab.com/ossicle/ossicle.js"
const library = "https://ossicle.gitlab.io/ossicle.js/Ossicle.min.js"


class MainTitle extends Ossicle {

  static sheets() { return [
    Style.css`
      .image.is-256x256 {
          height: 256px;
          width: 256px;
      }
    `,
    Style.sheets.bulma
  ]}

  static template() { return Template.html`
    <section class="hero is-bold"> <!--  is-medium is-light -->
      <div class="hero-body">
        <div class="container">
          <figure class="image is-256x256">
              <img src="./components/ossicle-600-600.png">
          </figure>
          <!--
          <h1 class="title is-1">
            ${mainTitle}
          </h1>
          -->
          <h2 class="subtitle">
            ${subTitle}
          </h2>
          <h2 class="subtitle">
            🦊 Project: <a href="${project}">${project}</a> | 📦 Download: <a href="${library}">${library}</a>
          </h2>
        </div>
      </div>
    </section>
  `
  }

  constructor() {
    super()
    //this.setup(template.clone(), [Style.sheets.bulma, sheetImageSizes])
    this.setup()
  }
}

customElements.define('main-title', MainTitle)
