import {Style, Template, Fragment, Ossicle} from '../node_modules/@ossicle/ossicle.js/Ossicle.js'
import {Data} from '../content/Data.js'


class TabsBar extends Ossicle {

  static sheets() { return [
    Style.css`
      a {
        font-size:1.2em;
      }
    `,
    Style.sheets.bulma
  ]}

  static template() { return Template.html`
      <div class="container">
        <!-- Tabs -->
        <div class="tabs">
          <ul>

          ${Data.docSections.map(section => Fragment.html`
            <li id="${section.id}" class="tab is-active">
              <a href="#/${section.id}">${section.title}</a>
            </li>
          `).join("")}

          </ul>
        </div>
        <!-- End of Tabs -->
      </div>
    `
  }



  activate(id) {
    this.$classes("tab").forEach(tab => tab.className = "tab")
    this.$id(id).className += " is-active"
  }

  constructor() {
    super()
    //this.setup(template.clone(), [Style.sheets.bulma, sheetTab])
    this.setup()
  }
}

customElements.define('tabs-bar', TabsBar)
