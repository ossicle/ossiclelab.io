# 🛠 Attributes

🖐️ The use of attributes **is not a specific feature of Ossicle, it's just vanilla JavaScript/HTML**

### MainApplication.js

I want to define the title (1️⃣) and the subtitle (2️⃣) of **<main-title></main-title>** with attributes:

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title 1️⃣ title="Hello World" 2️⃣ sub-title="with Ossicle"></main-title>
    </div>

    <div class="section">
      <p>
        Hello from the "MainApplication"
      </p>
    </div>

  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }
}

customElements.define('main-application', MainApplication)
```

### MainTitle.js

Then, to get the values of the attributes of **<main-title></main-title>**, you only need to use the **getAttribute(attributeName)** method of the element: see 1️⃣ and 2️⃣

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div>
    <h1 class="title orange"></h1>
    <h2 class="subtitle green"></h2>
  </div>
`

class MainTitle extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma, Style.sheets.colors])
  }

  connectedCallback() {
    this.$("h1").innerText = this.getAttribute("title") 1️⃣
    this.$("h2").innerText = this.getAttribute("sub-title") 2️⃣
  }
}

customElements.define('main-title', MainTitle)
```

This is possible too:

```javascript
this.$("div").appendChild(Template.html`
  <h1 class="title orange">${this.getAttribute("title")}</h1>
  <h2 class="subtitle green">${this.getAttribute("sub-title")}</h2>
`.clone())
```


> 🖐️ you can get the source code here: [https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/attributes](https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/attributes)

# 🛠 [Alternative] Attributes

**MainApplication**
```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

class MainApplication extends Ossicle {
  static sheets() { return [Style.sheets.bulma] }

  static template() { return Template.html`
    <div class="container mb-2 pt-2">
      <div class="section">
        <main-title title="Hello World" sub-title="with Ossicle"></main-title>
      </div>
      <div class="section">
        <p>
          Hello from the "MainApplication"
        </p>
      </div>
    </div>
  `}
  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-application', MainApplication)
```

**MainTitle**
```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

class MainTitle extends Ossicle {
  static sheets() { return [Style.sheets.bulma, Style.sheets.colors] }

  static template() { return Template.html`
    <div>
      <h1 class="title orange"></h1>
      <h2 class="subtitle green"></h2>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }

  connectedCallback() {
    this.$("h1").innerText = this.getAttribute("title")
    this.$("h2").innerText = this.getAttribute("sub-title")
  }
}

customElements.define('main-title', MainTitle)
```
