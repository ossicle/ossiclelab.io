# 🎨 Styles

You can style separately every component by using **adoptedStyleSheets**.

## adoptedStyleSheets

The **Style.css** helper generates an **adoptedStyleSheet**

> - see [https://wicg.github.io/construct-stylesheets/](https://wicg.github.io/construct-stylesheets/)
> - and [https://developers.google.com/web/updates/2019/02/constructable-stylesheets](https://developers.google.com/web/updates/2019/02/constructable-stylesheets))

```javascript
import {Style 1️⃣, Template, Ossicle} from '../js/Ossicle.js'

const titlesSheet = Style.css` 2️⃣
  h1 { color: orange; }
  h2 { color: green; }
`

const template = Template.html`
  <div>
    <h1>👋 Hello World 🌍</h1>
    <h2>with Ossicle.js</h2>
  </div>
`

class MainTitle extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [titlesSheet]) 3️⃣ 4️⃣
  }
}

customElements.define('main-title', MainTitle)
```

- 1️⃣ import the **Style** helper
- 2️⃣ generate an **adoptedStyleSheet**
- 3️⃣ add it to the component (the parameter is an array: you can add several adoptedStyleSheets to the component)
- 4️⃣ this notation (**setup()**) is a shortcut for:
  ```javascript
  this.appendChild(template.clone())
  this.adoptedStyleSheets([titlesSheet])
  ```

## Share an "adoptedStyleSheet"

If you want to share an "adoptedStyleSheet" with the other components, you can "add" as a property of **Style.sheets**:

```javascript
const titlesSheet = Style.css` 2️⃣
  h1 { color: orange; }
  h2 { color: green; }
`
Style.sheets.titlesSheet = titlesSheet
```

And now, you can use it on every Ossicle components:

```javascript
this.setup(template.clone(), [Style.sheets.titlesSheet])
```

## 🖐️ If you don't use Chrome

You need to add this polyfill: [https://unpkg.com/construct-style-sheets-polyfill@2.4.3/dist/adoptedStyleSheets.js](https://unpkg.com/construct-style-sheets-polyfill@2.4.3/dist/adoptedStyleSheets.js)

```html
<!-- polyfill (FireFox)-->
<script src="./js/adoptedStyleSheets.js"></script>
```

> 🖐️ you can get the source code here: [https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/styles](https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/styles)

# [Alternative] 🎨 Styles

**MainApplication**
```javascript
import {Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

class MainApplication extends Ossicle {
  static template() { return Template.html`
    <div>
      <main-title></main-title>
      <div>
        Hello from the "MainApplication"
      </div>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-application', MainApplication)
```

**MainTitle**
```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

class MainTitle extends Ossicle {

  static sheets() { return [
    Style.css`
      h1 { color: orange; }
      h2 { color: green; }
    `
  ]}

  static template() { return Template.html`
    <div>
      <h1>👋 Hello World 🌍</h1>
      <h2>with Ossicle.js</h2>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-title', MainTitle)
```