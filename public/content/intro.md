# 👋 Hello World 🌍

**Ossicle** is a simple set of helpers for construct web application based on Web Components. Only some minutes are necessary to learn **Ossicle**, and the rest is only vanilla JavaScript and HTML.

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

const sheetTitles = Style.css`
  h1 { color: orange; }
  h2 { color: green; }
`

const template = Template.html`
  <div>
    <h1>👋 Hello World 🌍</h1>
    <h2>with Ossicle.js</h2>
  </div>
`

class HelloWorld extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [sheetTitles])
  }
}

customElements.define('hello-world', HelloWorld)
```

## Alternative notation (static methods)

🖐️ You can use static methods to define styles and html template:

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

class HelloWorld extends Ossicle {

  static sheets() { return [
    Style.css`
      h1 { color: orange; }
      h2 { color: green; }
    `
  ]}

  static template() { return Template.html`
    <div>
      <h1>👋 Hello World 🌍</h1>
      <h2>with Ossicle.js</h2>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('hello-world', HelloWorld)
```
