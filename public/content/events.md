# 🔥 Events

The easiest way to handle events is to use the **addEventListener** method of the HTML elements. In the following example, we want to change the text of the main title when clicking on a button.

🖐️ The use of the **addEventListener** method **is not a specific feature of Ossicle, it's just vanilla JavaScript/HTML**

## addEventListener

### NiceButton.js

Create a new **NiceButton** Ossicle component:

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <button class="button is-info">
    <slot></slot>
  </button>
`

class NiceButton extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }
}

customElements.define('nice-button', NiceButton)
```

### MainTitle.js

Change the code of the **MainTitle** component:

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div>
    <h1 class="title">
      👋 <slot></slot>
    </h1>
  </div>
`

class MainTitle extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }
}

customElements.define('main-title', MainTitle)
```

### MainApplication.js

Change the code of the **MainApplication** component:

- 1️⃣ import the NiceButton component
- 2️⃣ add two **<nice-button>** elements and give a **name** to each button
- 3️⃣ search the button by its name and add a **click even listener** to it
- 4️⃣ change the **innerText** property of the **<main-title>** element *(this will update the value of the <slot> element)*

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'
import './NiceButton.js' 1️⃣

const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title>Hello World</main-title>
    </div>

    <div class="section">
      <nice-button name="btn1">one</nice-button> 2️⃣
      <nice-button name="btn2">two</nice-button> 2️⃣
    </div>

  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {
    this.$name("btn1").addEventListener("click", _ => { 3️⃣
      // update the slot content of <main-title>
      this.$("main-title").innerText = "You clicked button one" 4️⃣
    })

    this.$name("btn2").addEventListener("click", _ => { 3️⃣
      // update the slot content of <main-title>
      this.$("main-title").innerText = "You clicked button two" 4️⃣
    })

  }
}

customElements.define('main-application', MainApplication)
```

## Call a method or a property of an object

An other way could be to call a property (or a method) of the **<main-title>** element:

### Change MainTitle.js

1️⃣ Add a **title** setter to the component:

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div>
    <h1 class="title">👋 <slot></slot></h1>
  </div>
`

class MainTitle extends Ossicle {

  set title(value) { 1️⃣
    this.innerText = value
  }

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }
}

customElements.define('main-title', MainTitle)
```

### Change MainApplication.js

1️⃣ now you can directly call the **title** property of the **<main-title>** element:

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'
import './NiceButton.js'

const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title>Hello World</main-title>
    </div>

    <div class="section">
      <nice-button name="btn1">one</nice-button>
      <nice-button name="btn2">two</nice-button>
    </div>

  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {
    this.$name("btn1").addEventListener("click", _ => {
      this.$("main-title").title = "You clicked button one 👋" 1️⃣
    })

    this.$name("btn2").addEventListener("click", _ => {
      this.$("main-title").title =  "You clicked button two 👋" 1️⃣
    })

  }
}

customElements.define('main-application', MainApplication)
```

> 🖐️ you can get the source code here: [https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/events](https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/events)
