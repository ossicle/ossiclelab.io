let Data = {
  docSections: [
    {id:"intro", file:"./content/intro.md", title:"👋 Hello World 🌍"},
    {id:"start", file:"./content/initialize.md", title:"🚀 Start"},
    {id:"first", file:"./content/first.md", title:"🧩 First component"},
    {id:"styles", file:"./content/styles.md", title:"🎨 Styles"},
    {id:"css", file:"./content/css.md", title:"🖼 Css stylesheets"},
    {id:"selectors", file:"./content/selectors.md", title:"🔎 Selectors"},
    {id:"attributes", file:"./content/attributes.md", title:"🛠 Attributes"},
    {id:"slots", file:"./content/slots.md", title:"🪣 Slots"},
    {id:"events", file:"./content/events.md", title:"🔥 Events"},
    {id:"communication", file:"./content/communication.md", title:"📟 Communication"},
    {id:"router", file:"./content/router.md", title:"🚙 Router"},
    {id:"recipes", file:"./content/recipes.md", title:"🍲 Recipes"}
    //{id:"samples", file:"./content/samples.md", title:"🎁 Samples"}
  ]
}

export {Data}
