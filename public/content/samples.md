# 🎁 Samples

Read the source code of the current page: [https://gitlab.com/ossicle/ossicle.gitlab.io](https://gitlab.com/ossicle/ossicle.gitlab.io) or see the samples used for the documentation [https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples](https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples)
