# 🪣 Slots

🖐️ The use of the Slot API and **<slot>** element **is not a specific feature of Ossicle, it's just vanilla JavaScript/HTML**

> 📘 See:
> - [https://developer.mozilla.org/en-US/docs/Web/HTML/Element/slot](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/slot)
> - [https://developer.mozilla.org/en-US/docs/Web/API/HTMLSlotElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLSlotElement)

Slots are useful if you want to add content **"directly"** in your web component:

## Add content to a component

Here (1️⃣) I add this sentence **"this is an amazing web application 😉"** to my component:

```javascript
const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title title="Hello World" sub-title="with Ossicle">
        1️⃣ this is an amazing web application 😉
      </main-title>
    </div>
  </div>
`
```

### Render the content

You only need to add a **<slot>** tag (1️⃣) to the template of your component to be able to render it's content.

#### MainTitle.js

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div>
    <h1 class="title orange"></h1>
    <h2 class="subtitle green"></h2>
    <hr>
      <slot></slot> 1️⃣
    <hr>
  </div>
`

class MainTitle extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma, Style.sheets.colors])
  }

  connectedCallback() {
    this.$("h1").innerText = this.getAttribute("title")
    this.$("h2").innerText = this.getAttribute("sub-title")
  }
}

customElements.define('main-title', MainTitle)
```

## Use named slots

### Add a named slot

If you need several slots, you have to name the slots.

####  MainTitle.js

I added a new slot named **"bold-slot"** (1️⃣):

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div>
    <h1 class="title orange"></h1>
    <h2 class="subtitle green"></h2>
    <hr>
      <slot></slot>
    <hr>
    <b><slot name="bold-slot"></slot></b> 1️⃣
  </div>
`

class MainTitle extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma, Style.sheets.colors])
  }

  connectedCallback() {
    this.$("h1").innerText = this.getAttribute("title")
    this.$("h2").innerText = this.getAttribute("sub-title")
  }
}

customElements.define('main-title', MainTitle)
```

### Use it

When adding our component in a template, we have the content of the first slot (1️⃣) and we use a **slot** attribute (2️⃣) with the name of the second slot to make the link between it and the content:

```javascript
const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title title="Hello World" sub-title="with Ossicle">
        1️⃣ this is an amazing web application 😉
        2️⃣ <span slot="bold-slot">this is a bold sentence</span>
      </main-title>
    </div>
  </div>
`
```

> 🖐️ you can get the source code here: [https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/slots](https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/slots)
