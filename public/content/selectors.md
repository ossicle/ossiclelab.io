# 🔎 Selectors

An Ossicle element comes with some helpers to ease the walk through the shadow dom.

*🚧 W.I.P.*

### search a tag

```javascript
this.$("div") // return a baseElement
```

> See [https://developer.mozilla.org/en-US/docs/Web/API/Element/querySelector](https://developer.mozilla.org/en-US/docs/Web/API/Element/querySelector)


### search all tags

```javascript
this.$all("div") // return a NodeList
```

> See [https://developer.mozilla.org/en-US/docs/Web/API/Element/querySelectorAll](https://developer.mozilla.org/en-US/docs/Web/API/Element/querySelectorAll)

### search an element by class name

```javascript
this.$class("title")
```

### search all element with a specific class name

```javascript
this.$classes("title")
```

### search an element by name

```javascript
this.$name("amazing-title")
```

### search an element by id

```javascript
this.$id("amazing-title")
```

