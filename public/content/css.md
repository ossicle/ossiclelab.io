
# 🖼 Use style sheets with external css files

If you don't write your own css styles, you would like to style your application with a ready-to-use css framework, like the lovely **[Bulma](https://bulma.io/)**. It's very easy to do with Ossicle.

## Add css stylesheets to your project

```bash
.
├── components
│  ├── MainApplication.js
│  └── MainTitle.js
├── css
│  ├── bulma.min.css 1️⃣
│  └── colors.css 2️⃣
├── index.html
└── js
   └── Ossicle.js
```

- 1️⃣ I use Bulma
- 2️⃣ I added my own style sheet **colors.css**
  ```css
  .orange { color: orange; }
  .green { color: green; }
  ```

## Change the initialization of the web application

### index.html

```html
<!doctype html>
<html lang="en">
<head>
  <title>Ossicle</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
</head>
  <body>
    <!-- Ossicle Web Component -->
    <main-application></main-application>
  </body>

  <!-- Initialize and Load -->
  <script type="module">
    import {Initialize} from "./js/Ossicle.js" 1️⃣

    Initialize.css(
      [
        {sheet:"./css/bulma.min.css", name:"bulma", apply:true}, 2️⃣
        {sheet:"./css/colors.css", name:"colors", apply:false} 3️⃣
        // apply:false then it doesn't apply to the whole document
      ]
    ).then(_ => {
      import("./components/MainApplication.js").then( _ => { 4️⃣
        console.log("👋 MainApplication is loaded")
      })
    })
  </script>
</html>
```

- 1️⃣ import the **Initialize** helper
- 2️⃣ load the Bulma css file and give it a name (**apply:true** means that the Bulma styles apply to the whole document)
- 3️⃣ load the colors css file
- 4️⃣ once the css files loaded, you can load your main component

The **Initialize.css** helper creates 2 adoptedStyleSheets, and you can get them from **Style.sheets**:

```javascript
Style.sheets.bulma
Style.sheets.colors
```

## Use bulma with MainApplication.html

```javascript
import {Style 1️⃣, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

const template = Template.html`
  <div class="container mb-2 pt-2"> 2️⃣
    <div class="section"> 2️⃣
      <main-title></main-title>
    </div>
    <div class="section"> 2️⃣
      <p>
        Hello from the "MainApplication"
      </p>
    </div>
  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma]) 3️⃣
  }
}

customElements.define('main-application', MainApplication)
```

- 1️⃣ import the **Style** helper
- 2️⃣ use Bulma styles
- 3️⃣ add the adoptedStyleSheet to the component

## Use bulma and colors with MainTitle.html

```javascript
import {Style 1️⃣, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div>
    <h1 class="title orange">👋 Hello World 🌍</h1> 2️⃣
    <h2 class="subtitle green">with Ossicle.js</h2> 2️⃣
  </div>
`

class MainTitle extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma, Style.sheets.colors]) 3️⃣
  }
}

customElements.define('main-title', MainTitle)
```

- 1️⃣ import the **Style** helper
- 2️⃣ use Bulma styles (**title** and **subtitle**) and colors styles (**orange** and **green**)
- 3️⃣ add the 2 adoptedStyleSheets to the component

## 🖐️ If you don't use Chrome

You need to add this polyfill: [https://unpkg.com/construct-style-sheets-polyfill@2.4.3/dist/adoptedStyleSheets.js](https://unpkg.com/construct-style-sheets-polyfill@2.4.3/dist/adoptedStyleSheets.js)

```html
<!-- polyfill (FireFox)-->
<script src="./js/adoptedStyleSheets.js"></script>
```

> 🖐️ you can get the source code here: [https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/css](https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/css)

# 🖼 [Alternative] Use style sheets with external css files

**MainApplication**
```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

class MainApplication extends Ossicle {

  static sheets() { return [
    Style.sheets.bulma
  ]}

  static template() { return Template.html`
    <div class="container mb-2 pt-2">
      <div class="section">
        <main-title></main-title>
      </div>
      <div class="section">
        <p>
          Hello from the "MainApplication"
        </p>
      </div>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-application', MainApplication)
```

**MainTitle**
```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

class MainTitle extends Ossicle {

  static sheets() { return [
    Style.sheets.bulma, Style.sheets.colors
  ]}

  static template() { return Template.html`
    <div>
      <h1 class="title orange">👋 Hello World 🌍</h1>
      <h2 class="subtitle green">with Ossicle.js</h2>
    </div>
  `}  

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-title', MainTitle)
```