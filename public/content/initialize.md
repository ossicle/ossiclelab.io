# 🚀 Initialize an Ossicle web application

## Install

Download **Ossicle** here: [https://ossicle.gitlab.io/ossicle.js/Ossicle.js](https://ossicle.gitlab.io/ossicle.js/Ossicle.js) or [https://ossicle.gitlab.io/ossicle.js/Ossicle.min.js](https://ossicle.gitlab.io/ossicle.js/Ossicle.min.js)

### Install with npm

```bash
echo @ossicle:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npm init
npm install @ossicle/ossicle.js
```

## Project structure

Create a project structure like this:

```bash
.
├── components
│  └── MainApplication.js
├── index.html
└── js
   └── Ossicle.js
```

### index.html

```html
<!doctype html>
<html lang="en">
<head>
  <title>Ossicle</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
</head>
  <body>
    <!-- Ossicle Web Component -->
    <main-application></main-application>
  </body>

  <!-- Initialize and Load -->
  <script type="module">
    import("./components/MainApplication.js")
  </script>
</html>
```

### MainApplication.js

```javascript
import {Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <h1>
    👋 Hello World 🌍
  </h1>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.appendChild(template.clone())
  }
}

customElements.define('main-application', MainApplication)
```

#### 🖐️ Alternative notation

```javascript
import {Template, Ossicle} from '../js/Ossicle.js'

class MainApplication extends Ossicle {
  static template() { return Template.html`
    <h1>
      👋 Hello World 🌍
    </h1>
  `}

  constructor() {
    super()
    this.setup() // template.clone()
  }
}

customElements.define('main-application', MainApplication)
```

## Why Template?

The Template API creates neutral HTML templates in our elements (*🖐️ it's not a templating feature like with Mustache*). **Template.html** is an Ossicle helper that allows to define and create a **template** object. The template is defined and created outside the component, and then when instantiating the component we use a clone of this template. Then even if we create 100 **<main-application>** elements, there will be only one template creation 😃.

📘 ref: [https://developer.mozilla.org/en-US/docs/Web/HTML/Element/template](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/template)

> 👋 another way would be to use a static property of the component to store the template.

## 🎉 serve your project

You can use **[http-server](https://www.npmjs.com/package/http-server)**. Run the below command from the root of your project:

```
http-server

# then you can reach `http://127.0.0.1:8080` with your browser.
```


> 🖐️ you can get the source code here: [https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/start](https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/start)
