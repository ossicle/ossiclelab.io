# 📟 Communication

*🚧 W.I.P.*

In the **[events](/#/events)** section we saw how to use the **addEventListener** of the elements to listen on a **click** event triggered by a click on a button. But you can dispatch your own cusstom events to communicate between components. And it's very easy.

You can use the **<main-application>** element to listen all the events from the embedded elements.

## Change NiceButton.js

- 1️⃣ add a click handler to the NiceButton components
- 2️⃣ send a **"hey-title"** event with a json payload 3️⃣

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <button class="button is-info">
    <slot></slot>
  </button>
`

class NiceButton extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {
    this.addEventListener("click", _ => { 1️⃣
      this.sendEvent(
        "hey-title", 2️⃣
        {text:`You clicked button ${this.getAttribute("name")}`} 3️⃣
      )
    })
  }

}

customElements.define('nice-button', NiceButton)
```

🖐️ the **sendEvent** is an Ossicle helper to create and dispatch a new **CustomEvent**

> - the listener will receive a message with a structure like that: **{ detail: { text:"text of the message" } }**
> - 📘 ref:
>    - [https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent)
>    - [https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Creating_and_triggering_events](https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Creating_and_triggering_events)


## Change MainApplication.js (the listener)

1️⃣ the **<main-application>** element is listening for a **"hey-title"** event, and when this event is fired by a **<nice-button>** element, the main element "tell" the **<main-title>** element to change its title text with the text property of the message: **this.$("main-title").title = message.detail.text** 2️⃣

```javascript
import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'
import './NiceButton.js'

const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title>Hello World</main-title>
    </div>

    <div class="section">
      <nice-button name="btn1">one</nice-button>
      <nice-button name="btn2">two</nice-button>
    </div>

  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {

    this.addEventListener("hey-title", message => { 1️⃣
      console.log("👋", message)
      this.$("main-title").title = message.detail.text 2️⃣
    })
  }
}

customElements.define('main-application', MainApplication)
```

> 🖐️ you can get the source code here: [https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/communication](https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/communication)
