# 🚙 Router

The **Router** helper of Ossicle is very simple to use:

- 1️⃣ import the **Router** helper
- 2️⃣ define links like that: **#/my_link**
- 3️⃣ start the router with a callback
- 4️⃣ when the <main-application> is loaded, the event **type** is **load** otherwise it's **hash**
- 5️⃣ the possible values of **location** are **/**, **/one**, **/two** and **/three**

```javascript
import {Style, Template, 1️⃣ Router, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

const template = Template.html`
  <div class="container mb-2 pt-2">
    <div class="section">
      <main-title>Hello World</main-title>
    </div>

    <div class="section">
      <ul>
        <li><a href="#/one">One</a></li> 2️⃣
        <li><a href="#/two">Two</a></li> 2️⃣
        <li><a href="#/three">Three</a></li> 2️⃣
      </ul>
    </div>

  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {
    // start the router and call `onRouterEvent` at every `hash` event
    // the firs time the router triggers a `load` event
    Router.start(message => this.onRouterEvent(message)) 3️⃣
  }

  onRouterEvent(message) {
    console.log("type", message.type) 4️⃣
    console.log("location", message.location) 5️⃣
  }

}

customElements.define('main-application', MainApplication)
```
