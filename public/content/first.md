# 🧩 Add your first Ossicle component

You'll add a new Web Component to the web application.

## Create a new file MainTitle.js

```bash
.
├── components
│  ├── MainApplication.js
│  └── MainTitle.js
├── index.html
└── js
   └── Ossicle.js
```

With this code:

```javascript
import {Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div>
    <h1>👋 Hello World 🌍</h1>
    <h2>with Ossicle.js</h2>
  </div>
`

class MainTitle extends Ossicle {
  constructor() {
    super()
    this.appendChild(template.clone())
  }
}

customElements.define('main-title', MainTitle)
```

## Update MainApplication.js

```javascript
import {Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js' 1️⃣

const template = Template.html`
  <div>
    <main-title></main-title> 2️⃣
    <div>
      Hello from the "MainApplication"
    </div>
  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.appendChild(template.clone())
  }
}

customElements.define('main-application', MainApplication)
```

- 1️⃣ import the new component
- 2️⃣ add a new component: **MainTitle**


## 🎉 serve your project

```
http-server

# then you can reach `http://127.0.0.1:8080` with your browser.
```

## How to update "dynamically" the html content of my component?

For example, I would like to add a list to my component after its creation. We're going to use the **connectedCallback** method.

### Update MainApplication.js

```javascript
import {Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

const template = Template.html`
  <div>
    <main-title></main-title>
    <div>
      👋 Hello from the "MainApplication"
    </div>
    <list></list> 1️⃣
  </div>
`

class MainApplication extends Ossicle {
  constructor() {
    super()
    this.appendChild(template.clone())
  }

  connectedCallback() {
    var greetings = ["Hello", "Morgen", "Bonjour"]

    this.$("list").appendChild(Template.html` 2️⃣
        <ul id="first-list">
          ${greetings.map(item => `<li>${item}</li>`).join("")}
        </ul>
      `.clone()
    )
  }
}

customElements.define('main-application', MainApplication)
```
- 1️⃣ add a **list** tag
- 2️⃣ append a cloned template to **list** tag

#### Replace the cloned template by a new one

```javascript
greetings = ["Hey", "Yo", "Hi"]

this.$("list").replaceChild(Template.html`
    <ul id="new-list">
      ${greetings.map(item => `<li>${item}</li>`).join("")}
    </ul>
  `.clone(),
  this.$id("first-list")
)
```

#### Remove the clones template

```javascript
this.$("list").removeChild(this.$id("new-list"))
```

#### Remark: the quick way

Of course, you can directly change the value of the **innerHTML** property:

```javascript
this.$("list").innerHTML = this.greetings.map(item => `
  <li>${item}</li>
`).join("")
```

> remark: **this.$("list")** is a shortcut for **this.shadowRoot.querySelector("list")**

> 🖐️ you can get the source code here: [https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/first](https://gitlab.com/ossicle/ossicle.gitlab.io/-/tree/master/samples/first)


# 🧩 [Alternative] Add your first Ossicle component - Bis

**MainApplication**
```javascript
import {Template, Ossicle} from '../js/Ossicle.js'
import './MainTitle.js'

class MainApplication extends Ossicle {

  static template() { return Template.html`
    <div>
      <main-title></main-title>
      <div>
        👋 Hello from the "MainApplication"
      </div>
      <list></list>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }

  connectedCallback() {
    var greetings = ["Hello", "Morgen", "Bonjour"]

    this.$("list").appendChild(Template.html`
        <ul id="first-list">
          ${greetings.map(item => `<li>${item}</li>`).join("")}
        </ul>
      `.clone()
    )
  }
}

customElements.define('main-application', MainApplication)
```

**MainTitle**
```javascript
import {Template, Ossicle} from '../js/Ossicle.js'

class MainTitle extends Ossicle {

  static template() { return Template.html`
    <div>
      <h1>👋 Hello World 🌍</h1>
      <h2>with Ossicle.js</h2>
    </div>
  `}

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-title', MainTitle)
```